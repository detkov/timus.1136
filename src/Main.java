import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static PrintWriter printWriter = new PrintWriter(System.out);

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int N = sc.nextInt();
        int[] parliamentarians = new int[N];
        for (int i = 0; i < N; i++)
            parliamentarians[i] = sc.nextInt();

        Seat head = new Seat(parliamentarians[N - 1], true);

        for (int i = N - 2; i >= 0; i--)
            head.insert(parliamentarians[i]);

        head.printEven();

        printWriter.close();
        printWriter.flush();
    }
}

class Seat {
    private int index;
    private Seat great;
    private Seat less;
    boolean root;

    Seat(int index, boolean root) {
        this.index = index;
        this.root = root;
    }

    public void insert(int index) {
        if (index < this.index) {
            if (less == null) less = new Seat(index, false);
            else less.insert(index);
        } else {
            if (great == null) great = new Seat(index, false);
            else great.insert(index);
        }
    }

    public void printEven() {
        if (great != null) great.printEven();
        if (less != null) less.printEven();
        if (root) Main.printWriter.println(index);
        else Main.printWriter.print(index + " ");
    }
}
